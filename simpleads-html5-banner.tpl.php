<?php
/**
 * @file
 */
?>
<div class="simplead-container html5-banner-ad <?php if (isset($css_attributes)): print $css_attributes; endif; ?>">
   <iframe class="html-banner" scrolling="no" frameborder="0" src="<?php print $iframe_attr['src']; ?>"
    width="<?php print $iframe_attr['width'] ?>" height="<?php print $iframe_attr['height'] ?>"> </iframe>
  <?php print l('#', $ad['url'], $link_attributes); ?>
</div>
