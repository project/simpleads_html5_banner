<?php
/**
 * @file
 * Script for simpleads html5 banner module.
 */

/**
 * Implements hook_js_alter().
 */
function simpleads_html5_banner_js_alter(&$js) {
  $js_simpleads_path = drupal_get_path('module', 'simpleads') .'/simpleads.js';
  // change path for js in admin content.
  if (isset($js[$js_simpleads_path])) {
    $js[$js_simpleads_path]['data'] = drupal_get_path('module', 'simpleads_html5_banner') .'/simpleads.js';
  }
}

/**
 * Implements hook_theme().
 */
function simpleads_html5_banner_theme() {
  return array(
    'simpleads_html5_banner_element' => array(
      'variables' => array('ad' => NULL, 'settings' => NULL, 'css_class' => NULL),
      'template' => 'simpleads-html5-banner',
      'preprocess functions' => array(
        'simpleads_preprocess_simpleads_html5_banner_element',
      ),
    ),
  );
}

function simpleads_preprocess_simpleads_html5_banner_element(&$variables) {
  // add css.
  drupal_add_css(drupal_get_path('module', 'simpleads_html5_banner') . '/simpleads-html5-banner.css', array('group' => CSS_DEFAULT, 'type' => 'file'));

  $iframe_attr = array();
  $variables = _simpleads_theme_attributes_init($variables);
  _simpleads_increase_impression($variables['ad']['nid']);

  $iframe_attr['width'] = 100;
  $iframe_attr['height'] = 100;
  if (isset($variables['settings']['ads_width']) && is_numeric($variables['settings']['ads_width'])) {
    $iframe_attr['width'] = check_plain($variables['settings']['ads_width']);
  }
  if (isset($variables['settings']['ads_height']) && is_numeric($variables['settings']['ads_height'])) {
    $iframe_attr['height'] = check_plain($variables['settings']['ads_height']);
  }

  $src_link = 'public://html5_banner/banner_' . $variables['ad']['nid'] . '/index.html';
  $iframe_attr['src'] = file_create_url($src_link);

  // Link attributes
  $link_attributes['html'] = TRUE;
  if ($variables['ad']['target'] && !user_access('administer nodes')) {
    $link_attributes['attributes']['target'] = '_blank';
  }
  $variables['link_attributes'] = $link_attributes;
  $variables['iframe_attr'] = $iframe_attr;
}

/**
 * Helper function, that rename file for node in drupal.
 */
function _simpleads_html5_banner_update_file_name($node, $field_name = 'field_html5_banner') {
  //@TODO make sure language.
  if (!empty($node->{$field_name}[LANGUAGE_NONE][0]['fid'])) {
    $fid = $node->{$field_name}[LANGUAGE_NONE][0]['fid'];
    $file = file_load($fid);
    $old_file = clone $file;
    // move file into news.
    $config = field_info_instance('node', $field_name, $node->type);
    // update manage data for file.
    $file->filename = preg_replace('/\d{4,}_/', '_', $file->filename, 1);
    $file->filename = REQUEST_TIME . '_'. $file->filename;

    $file_schema = file_uri_scheme($file->uri);
    $path = file_uri_scheme($file->uri) . '://' . ( empty($config['settings']['file_directory']) ? '' :
          $config['settings']['file_directory']);

    $file->uri = $path . '/'  . $file->filename;

    file_move($old_file, $file->uri);
    file_save($file);
  }
}

/**
 * Function helper for extract file.
 * This using for extract html5_banner.
 * @param $uri URI of file that could extract.
 * @param string $path the path that is containt file after this extract.
 */
function _simpleads_html5_banner_extract_html5_banner_file($uri, $extract_to = 'public://html5_banner/') {
  if (empty($extract_to)) {
    $extract_to = 'public://html5_banner/';
  }

  // check
  $obj_archiver = archiver_get_archiver($uri);
  if ( $obj_archiver ) {
    $obj = $obj_archiver->extract($extract_to);
  }
}


/**
 * Implements hook_node_update().
 */
function simpleads_html5_banner_node_update($node) {
  if (!empty($node->field_html5_banner[LANGUAGE_NONE][0]['fid'])) {
    /// get fid and attach file.
    $file = file_load($node->field_html5_banner[LANGUAGE_NONE][0]['fid']);
    $extra_to = 'public://html5_banner/banner_' . $node->nid;
    _simpleads_html5_banner_extract_html5_banner_file($file->uri, $extra_to);
  }
}

/**
 * Implements hook_node_presave().
 */
function simpleads_html5_banner_node_presave($node) {
  if ($node->type == 'simpleads') {
    _simpleads_html5_banner_update_file_name($node);
  }
}

/**
 * Implements hook_node_insert().
 * Update number of Practical sheets When inserting a Practical Sheet node on the Practical Sheet Category that current node belong to
 */
function simpleads_html5_banner_node_insert($node) {
  if (!empty($node->field_html5_banner[LANGUAGE_NONE][0]['fid'])) {
    /// get fid and attach file.
    $file = file_load($node->field_html5_banner[LANGUAGE_NONE][0]['fid']);
    $extra_to = 'public://html5_banner/banner_' . $node->nid;
    _simpleads_html5_banner_extract_html5_banner_file($file->uri, $extra_to);
  }
}

/**
 * Implements hook_node_delete().
 * Inactive associate en cuisine activities include:
 * - Add, modify, favourite, comment recipe
 * - Add, modify, favourite, comment practical sheet
 */
function simpleads_html5_banner_node_delete($node) {
  if (!empty($node->field_html5_banner[LANGUAGE_NONE][0]['fid'])) {
    /// get fid and attach file.
    $exist_path = 'public://html5_banner/banner_' . $node->nid;
    file_unmanaged_delete_recursive($exist_path);
  }
}

/**
 * Implements hook {SIMPLEAD-TYPE}_simpleads_display().
 * @see
 */
function html5_banner_simpleads_display($ads, $settings, $css_class) {
  return theme('simpleads_html5_banner_element', array('ad' => $ads, 'settings' => $settings, 'css_class' => $css_class));
}
